variable "name" {
  description = ""
  type        = string
}

variable "vpc_id" {
  description = ""
  type        = string
}

variable "subnet_id" {
  description = ""
  type        = string
}

variable "associate_public_ip_address" {
  description = "Whether or not public IP must be assigned to the instance during creation"
  type        = bool
  default     = false
}

variable "instance_type" {
  description = "Instance type"
  type        = string
  default     = "t3a.micro"
}

variable "ami_id" {
  description = "Fix AMI ID used by Spot request. By default module seeks for the latest available Debian Stretch image"
  type        = string
  default     = null
}

variable "key_name" {
  description = "Name of an already registered public SSH key. Unless specifies created instance won't be reachable using SSH"
  type        = string
  default     = null
}

variable "user_data" {
  description = ""
  type        = string
  default     = null
}

variable "ebs_root_volume_type" {
  description = "The type of volume e.g. 'standard', 'gp2', 'io1', 'sc1', or 'st1'"
  type        = string
  default     = "standard"
}

variable "ebs_root_volume_size" {
  description = "The size of the volume in gigabytes"
  type        = number
  default     = 8
}

variable "ebs_root_volume_delete_on_termination" {
  description = "Whether the volume should be destroyed on instance termination"
  type        = bool
  default     = true
}

variable "advanced_monitoring" {
  description = "Whether or not advanced monitoring should be enabled. Additional charges apply"
  type        = bool
  default     = false
}

variable "max_spot_price" {
  description = "AWS determines spot price based on current demand, however it is possible to limit it max value"
  type        = string
  default     = null
}

variable "additional_security_group_ids" {
  description = "Any additional Security Group IDs to attach to the instance."
  type        = list(string)

  validation {
    condition     = length(var.additional_security_group_ids) <= 4
    error_message = "Only 5 security groups are allowed per network interface. One has been already taken by the module."
  }

  default = []
}

variable "extra_tags" {
  description = "Map of additional tags to add to module's resources"
  type        = map(string)
  default     = {}
}
