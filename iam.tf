resource "aws_iam_role" "ec2_instance" {
  name               = "${join("", regexall("[[:alnum:]]", var.name))}EC2Instance"
  assume_role_policy = data.aws_iam_policy_document.ec2_instance_assume_role_policy.json
  tags               = merge(local.common_tags, var.extra_tags)
}

data "aws_iam_policy_document" "ec2_instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_instance_profile" "ec2_instance" {
  name = "${join("", regexall("[[:alnum:]]", var.name))}EC2Instance"
  role = aws_iam_role.ec2_instance.name
  tags = merge(local.common_tags, var.extra_tags)
}
