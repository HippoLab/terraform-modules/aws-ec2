output "instance_id" {
  description = "Instance ID"
  value       = aws_spot_instance_request.ec2_instance.spot_instance_id
}

output "availability_zone" {
  value = aws_spot_instance_request.ec2_instance.availability_zone
}

output "public_ipv4_address" {
  value = aws_spot_instance_request.ec2_instance.public_ip
}

output "public_ipv6_addresses" {
  value = aws_spot_instance_request.ec2_instance.ipv6_addresses
}

output "sg_id" {
  description = "Default Security Group ID"
  value       = aws_security_group.ec2_instance_default_sg.id
}

output "role_arn" {
  description = "The ARN of the IAM Role"
  value       = aws_iam_role.ec2_instance.arn
}

output "role_name" {
  description = "The name of the IAM Role"
  value       = aws_iam_role.ec2_instance.name
}
