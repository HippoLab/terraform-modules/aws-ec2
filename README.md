Terraform AWS EC2 Spot instance
===============================

Terraform module to create following AWS resources:
- EC2 Spot Request
- Security Group

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Outputs](#outputs)
- [Licence](#licence)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
At least following input variables must be provided. See [full list](variables.tf) of supported variables

| Name      | Description                             |
| --------- | --------------------------------------- |
| name      | Common name - unique identifier         |
| vpc_id    | ID of a VPC resource will be created in |
| subnet_id | Subnet ID service will be attached to   |

## <a name="usage"></a> Usage

To provision default configuration containing one single subnet use:
```hcl-terraform
module "network" {
  source = "git::https://gitlab.com/HippoLab/terraform-modules/aws-network.git"
  name   = "${var.project_name} ${var.environment}"
}

module "gitlab_runner" {
  source                      = "git::https://gitlab.com/HippoLab/terraform-modules/aws-ec2.git"
  name                        = "${var.project_name} GitLab Runner"
  instance_type               = "t3a.medium"
  ami_id                      = "ami-0f1026b68319bad6c"
  vpc_id                      = module.network.vpc_id
  subnet_id                   = module.network.subnet_ids["public"][1]
  associate_public_ip_address = true
  ebs_root_volume_type        = "gp3"
  ebs_root_volume_size        = 16
  key_name                    = aws_key_pair.gitlab_runner.key_name
  extra_tags                  = local.common_tags
}

resource "tls_private_key" "gitlab_runner" {
  algorithm = "RSA"
}

resource "aws_key_pair" "gitlab_runner" {
  key_name   = "gitlab_runner"
  public_key = tls_private_key.gitlab_runner.public_key_openssh
}

resource "aws_security_group_rule" "gitlab_runner_ssh_ipv4_ingress" {
  security_group_id = module.gitlab_runner.sg_id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 22
  to_port           = 22
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  description = "Internet IPv4"
}

resource "aws_security_group_rule" "gitlab_runner_ssh_ipv6_ingress" {
  security_group_id = module.gitlab_runner.sg_id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 22
  to_port           = 22
  ipv6_cidr_blocks = [
    "::/0"
  ]
  description = "Internet IPv6"
}

resource "aws_security_group_rule" "gitlab_runner_ipv4_egress" {
  security_group_id = module.gitlab_runner.sg_id
  type              = "egress"
  protocol          = "all"
  from_port         = 0
  to_port           = 65535
  cidr_blocks = [
    "0.0.0.0/0"
  ]
}

resource "aws_security_group_rule" "gitlab_runner_ipv6_egress" {
  security_group_id = module.gitlab_runner.sg_id
  type              = "egress"
  protocol          = "all"
  from_port         = 0
  to_port           = 65535
  ipv6_cidr_blocks = [
    "::/0"
  ]
}
```

## <a name="outputs"></a> Outputs
Full list of module outputs and their descriptions can be found in [outputs.tf](outputs.tf)

## <a name="licence"></a> Licence
The module is being distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru><br/>London
