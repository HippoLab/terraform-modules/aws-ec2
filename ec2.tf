resource "aws_spot_instance_request" "ec2_instance" {
  instance_type        = var.instance_type
  ami                  = var.ami_id == null ? data.aws_ami.debian_buster.image_id : var.ami_id
  spot_type            = "persistent"
  spot_price           = var.max_spot_price
  wait_for_fulfillment = true
  key_name             = var.key_name
  user_data            = var.user_data
  monitoring           = var.advanced_monitoring

  vpc_security_group_ids = compact(concat([aws_security_group.ec2_instance_default_sg.id], var.additional_security_group_ids))

  subnet_id                       = var.subnet_id
  associate_public_ip_address     = var.associate_public_ip_address
  instance_interruption_behaviour = "stop"
  iam_instance_profile            = aws_iam_instance_profile.ec2_instance.name

  root_block_device {
    volume_type           = var.ebs_root_volume_type
    volume_size           = var.ebs_root_volume_size
    delete_on_termination = var.ebs_root_volume_delete_on_termination
  }

  tags = merge(
    {
      Name = var.name
    },
    local.common_tags,
    var.extra_tags
  )
}

// Tags are not being propagated to the created instance, therefore we apply them as a separate resource:

resource "aws_ec2_tag" "vpn_server" {
  for_each = merge(
    {
      Name = var.name
    },
    local.common_tags,
    var.extra_tags
  )
  resource_id = aws_spot_instance_request.ec2_instance.spot_instance_id
  key         = each.key
  value       = each.value
}

resource "aws_security_group" "ec2_instance_default_sg" {
  name                   = "${lower(replace(var.name, "/\\s/", "-"))}-ec2-instance"
  description            = "${var.name} Default EC2 Security Group"
  vpc_id                 = var.vpc_id
  revoke_rules_on_delete = true
  tags = merge(
    {
      Name = "${var.name} EC2 Instance"
    },
    local.common_tags,
    var.extra_tags
  )
}
