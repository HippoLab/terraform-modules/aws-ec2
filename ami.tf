data "aws_ami" "debian_buster" {
  owners = ["136693071363"] // Debian Buster account ID https://wiki.debian.org/Cloud/AmazonEC2Image

  filter {
    name = "name"
    values = [
      "debian-10-amd64-*"
    ]
  }

  most_recent = true
}

